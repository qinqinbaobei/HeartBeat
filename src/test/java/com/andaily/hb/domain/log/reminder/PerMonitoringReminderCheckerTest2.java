package com.andaily.hb.domain.log.reminder;

import com.andaily.hb.domain.application.ApplicationInstance;
import com.andaily.hb.domain.log.FrequencyMonitorLog;
import org.junit.Test;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;


/**
 * @author Shengzhao Li
 */
public class PerMonitoringReminderCheckerTest2 {


    /**
     * 前提: continueFailedTimes=2 (默认)
     * Test cases
     * 1. 无历史监控日志, 当前条 normal = true,  不发送提醒
     * 2 无历史监控日志, 当前条 normal = false,  不发送提醒
     * <p/>
     * 3 有一条历史监控日志(normal=true), 当前条 normal = false,  不发送提醒
     * 4 有一条历史监控日志(normal=true), 当前条 normal = true,  不发送提醒
     * 5 有一条历史监控日志(normal=false), 当前条 normal = true,  不发送提醒
     * 6 有一条历史监控日志(normal=false), 当前条 normal = false,  发送提醒
     * <p/>
     * 7 有两条历史监控日志(normal=false,normal=false), 当前条 normal = false,  不发送提醒
     * 8 有两条历史监控日志(normal=false,normal=false), 当前条 normal = true,  发送提醒
     * 9 有两条历史监控日志(normal=true,normal=true), 当前条 normal = true,  不发送提醒
     * 10 有两条历史监控日志(normal=true,normal=true), 当前条 normal = false,  不发送提醒
     * <p/>
     * 11  有两条历史监控日志(normal=true,normal=false), 当前条 normal = false,  不发送提醒
     * 12 有两条历史监控日志(normal=true,normal=false), 当前条 normal = true,  不发送提醒
     * 13 有两条历史监控日志(normal=false,normal=true), 当前条 normal = true,  不发送提醒
     * 14 有两条历史监控日志(normal=false,normal=true), 当前条 normal = false,  发送提醒
     *
     * @throws Exception
     */
    @Test
    public void testIsNeedReminder() throws Exception {

        ApplicationInstance instance = new ApplicationInstance().continueFailedTimes(2);
        //case 1
        FrequencyMonitorLog monitorLog1 = new FrequencyMonitorLog().normal(true).instance(instance);
        PerMonitoringReminderChecker checker1 = new PerMonitoringReminderChecker(monitorLog1) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return Collections.emptyList();
            }
        };
        final boolean need1 = checker1.isNeedReminder();
        assertFalse(need1);

        //case 2
        FrequencyMonitorLog monitorLog2 = new FrequencyMonitorLog().normal(false).instance(instance);
        PerMonitoringReminderChecker checker2 = new PerMonitoringReminderChecker(monitorLog2) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return Collections.emptyList();
            }
        };
        final boolean need2 = checker2.isNeedReminder();
        assertFalse(need2);

        //case 3
        FrequencyMonitorLog monitorLog3 = new FrequencyMonitorLog().normal(false).instance(instance);
        final List<FrequencyMonitorLog> historyList = new ArrayList<>();
        historyList.add(new FrequencyMonitorLog().normal(true).instance(instance));

        PerMonitoringReminderChecker checker3 = new PerMonitoringReminderChecker(monitorLog3) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return historyList;
            }
        };
        final boolean need3 = checker3.isNeedReminder();
        assertFalse(need3);


        //case 4
        FrequencyMonitorLog monitorLog4 = new FrequencyMonitorLog().normal(true).instance(instance);
        final List<FrequencyMonitorLog> historyList4 = new ArrayList<>();
        historyList4.add(new FrequencyMonitorLog().normal(true).instance(instance));

        PerMonitoringReminderChecker checker4 = new PerMonitoringReminderChecker(monitorLog4) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return historyList4;
            }
        };
        final boolean need4 = checker4.isNeedReminder();
        assertFalse(need4);


        //case 5
        FrequencyMonitorLog monitorLog5 = new FrequencyMonitorLog().normal(true).instance(instance);
        final List<FrequencyMonitorLog> historyList5 = new ArrayList<>();
        historyList5.add(new FrequencyMonitorLog().normal(false).instance(instance));

        PerMonitoringReminderChecker checker5 = new PerMonitoringReminderChecker(monitorLog5) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return historyList5;
            }
        };
        final boolean need5 = checker5.isNeedReminder();
        assertFalse(need5);


        //case 6
        FrequencyMonitorLog monitorLog6 = new FrequencyMonitorLog().normal(false).instance(instance);
        final List<FrequencyMonitorLog> historyList6 = new ArrayList<>();
        historyList6.add(new FrequencyMonitorLog().normal(false).instance(instance));

        PerMonitoringReminderChecker checker6 = new PerMonitoringReminderChecker(monitorLog6) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return historyList6;
            }
        };
        final boolean need6 = checker6.isNeedReminder();
        assertTrue(need6);


        //case 7
        FrequencyMonitorLog monitorLog7 = new FrequencyMonitorLog().normal(false).instance(instance);
        final List<FrequencyMonitorLog> historyList7 = new ArrayList<>();
        historyList7.add(new FrequencyMonitorLog().normal(false).instance(instance));
        historyList7.add(new FrequencyMonitorLog().normal(false).instance(instance));

        PerMonitoringReminderChecker checker7 = new PerMonitoringReminderChecker(monitorLog7) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return historyList7;
            }
        };
        final boolean need7 = checker7.isNeedReminder();
        assertFalse(need7);


        //case 8
        FrequencyMonitorLog monitorLog8 = new FrequencyMonitorLog().normal(true).instance(instance);
        final List<FrequencyMonitorLog> historyList8 = new ArrayList<>();
        historyList8.add(new FrequencyMonitorLog().normal(false).instance(instance));
        historyList8.add(new FrequencyMonitorLog().normal(false).instance(instance));

        PerMonitoringReminderChecker checker8 = new PerMonitoringReminderChecker(monitorLog8) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return historyList8;
            }
        };
        final boolean need8 = checker8.isNeedReminder();
        assertTrue(need8);


        //case 9
        FrequencyMonitorLog monitorLog9 = new FrequencyMonitorLog().normal(true).instance(instance);
        final List<FrequencyMonitorLog> historyList9 = new ArrayList<>();
        historyList9.add(new FrequencyMonitorLog().normal(true).instance(instance));
        historyList9.add(new FrequencyMonitorLog().normal(true).instance(instance));

        PerMonitoringReminderChecker checker9 = new PerMonitoringReminderChecker(monitorLog9) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return historyList9;
            }
        };
        final boolean need9 = checker9.isNeedReminder();
        assertFalse(need9);


        //case 10
        FrequencyMonitorLog monitorLog10 = new FrequencyMonitorLog().normal(false).instance(instance);
        final List<FrequencyMonitorLog> historyList10 = new ArrayList<>();
        historyList10.add(new FrequencyMonitorLog().normal(true).instance(instance));
        historyList10.add(new FrequencyMonitorLog().normal(true).instance(instance));

        PerMonitoringReminderChecker checker10 = new PerMonitoringReminderChecker(monitorLog10) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return historyList10;
            }
        };
        final boolean need10 = checker10.isNeedReminder();
        assertFalse(need10);


        //case 11   有两条历史监控日志(normal=true,normal=false), 当前条 normal = false,  不发送提醒
        FrequencyMonitorLog monitorLog11 = new FrequencyMonitorLog().normal(false).instance(instance);
        final List<FrequencyMonitorLog> historyList11 = new ArrayList<>();
        historyList11.add(new FrequencyMonitorLog().normal(true).instance(instance));
        historyList11.add(new FrequencyMonitorLog().normal(false).instance(instance));

        PerMonitoringReminderChecker checker11 = new PerMonitoringReminderChecker(monitorLog11) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return historyList11;
            }
        };
        final boolean need11 = checker11.isNeedReminder();
        assertFalse(need11);


        //case 12 有两条历史监控日志(normal=true,normal=false), 当前条 normal = true,  不发送提醒
        FrequencyMonitorLog monitorLog12 = new FrequencyMonitorLog().normal(true).instance(instance);
        final List<FrequencyMonitorLog> historyList12 = new ArrayList<>();
        historyList12.add(new FrequencyMonitorLog().normal(true).instance(instance));
        historyList12.add(new FrequencyMonitorLog().normal(false).instance(instance));

        PerMonitoringReminderChecker checker12 = new PerMonitoringReminderChecker(monitorLog12) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return historyList12;
            }
        };
        final boolean need12 = checker12.isNeedReminder();
        assertFalse(need12);


        //case 13  有两条历史监控日志(normal=false,normal=true), 当前条 normal = true,  不发送提醒
        FrequencyMonitorLog monitorLog13 = new FrequencyMonitorLog().normal(true).instance(instance);
        final List<FrequencyMonitorLog> historyList13 = new ArrayList<>();
        historyList13.add(new FrequencyMonitorLog().normal(false).instance(instance));
        historyList13.add(new FrequencyMonitorLog().normal(true).instance(instance));

        PerMonitoringReminderChecker checker13 = new PerMonitoringReminderChecker(monitorLog13) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return historyList13;
            }
        };
        final boolean need13 = checker13.isNeedReminder();
        assertFalse(need13);


        //case 14 有两条历史监控日志(normal=false,normal=true), 当前条 normal = false,  发送提醒
        FrequencyMonitorLog monitorLog14 = new FrequencyMonitorLog().normal(false).instance(instance);
        final List<FrequencyMonitorLog> historyList14 = new ArrayList<>();
        historyList14.add(new FrequencyMonitorLog().normal(false).instance(instance));
        historyList14.add(new FrequencyMonitorLog().normal(true).instance(instance));

        PerMonitoringReminderChecker checker14 = new PerMonitoringReminderChecker(monitorLog14) {
            @Override
            protected List<FrequencyMonitorLog> getLastLogsByCurrentLogs(int continueFailedTimes) {
                return historyList14;
            }
        };
        final boolean need14 = checker14.isNeedReminder();
        assertTrue(need14);


    }
}