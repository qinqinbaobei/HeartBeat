package com.andaily.hb.infrastructure;

import org.junit.Test;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;


/**
 * @author Shengzhao Li
 */
public class MD5Test {


    @Test
    public void encode() {
        Md5PasswordEncoder encoder = new Md5PasswordEncoder();
        String encode = encoder.encodePassword("honyee2013", null);
        System.out.println(encode);
    }

}