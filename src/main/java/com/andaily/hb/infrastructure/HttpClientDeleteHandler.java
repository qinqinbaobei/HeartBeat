package com.andaily.hb.infrastructure;

import org.apache.http.client.methods.RequestBuilder;

/**
 * 2020/3/17
 * <p>
 * DELETE
 *
 * @author Shengzhao Li
 * @since 2.0.1
 */
public class HttpClientDeleteHandler extends HttpClientHandler {
    public HttpClientDeleteHandler(String url) {
        super(url);
    }


    protected RequestBuilder createRequestBuilder() {
        return RequestBuilder.delete();
    }
}
