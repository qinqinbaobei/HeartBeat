package com.andaily.hb.domain.dto.application;

import com.andaily.hb.domain.application.*;
import com.andaily.hb.infrastructure.HttpClientHandler;
import org.apache.http.entity.ContentType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 15-1-4
 *
 * @author Shengzhao Li
 */
public class ApplicationInstanceFormDto extends ApplicationInstanceDto {


    private static final long serialVersionUID = 6377318702441859115L;
    private List<InstanceMonitorURLParameterDto> urlParameters = new ArrayList<>();

    private List<String> weixinUserGuids = new ArrayList<>();


    /**
     * 系统配置是否启用 proxy
     *
     * @since 2.0.1
     */
    private boolean proxyEnabled = false;

    public ApplicationInstanceFormDto() {
        super();
        //set default max connection time 30
        this.maxConnectionSeconds = 30;
    }

    public ApplicationInstanceFormDto(ApplicationInstance instance) {
        super(instance);

        initialURL(instance);
    }

    private void initialURL(ApplicationInstance instance) {
        final ApplicationInstanceURL instanceURL = instance.instanceURL();
        for (InstanceMonitorURLParameter urlParameter : instanceURL.urlParameters()) {
            this.urlParameters.add(new InstanceMonitorURLParameterDto(urlParameter));
        }
    }

    public List<String> getWeixinUserGuids() {
        return weixinUserGuids;
    }

    public void setWeixinUserGuids(List<String> weixinUserGuids) {
        this.weixinUserGuids = weixinUserGuids;
    }


    public List<InstanceMonitorURLParameterDto> getUrlParameters() {
        return urlParameters;
    }

    public int getUrlParametersSize() {
        return urlParameters.size();
    }

    public void setUrlParameters(List<InstanceMonitorURLParameterDto> urlParameters) {
        this.urlParameters = urlParameters;
    }


    public HeartBeatFrequency[] getFrequencies() {
        return HeartBeatFrequency.values();
    }


    public List<ContentType> getContentTypes() {
        return HttpClientHandler.CONTENT_TYPES;
    }


    public List<InstanceConnectType> getConnectTypes() {
        return Arrays.asList(InstanceConnectType.values());
    }

    public boolean isProxyEnabled() {
        return proxyEnabled;
    }

    public void setProxyEnabled(boolean proxyEnabled) {
        this.proxyEnabled = proxyEnabled;
    }

    /**
     * 更新domain 的信息
     */
    public ApplicationInstance updateDomain(ApplicationInstance instance) {
        instance.instanceURL().contentType(contentType);
        return instance.instanceName(instanceName)
                .monitorUrl(monitorUrl)
                .privateInstance(privateInstance)

                .requestMethod(requestMethod)
                .continueFailedTimes(continueFailedTimes)
                .maxConnectionSeconds(maxConnectionSeconds)
                .email(email)
                .frequency(frequency)

                .connectType(this.connectType)
                .requestBody(this.requestBody)
                .useProxy(this.useProxy)
                .remark(remark);
    }
}
