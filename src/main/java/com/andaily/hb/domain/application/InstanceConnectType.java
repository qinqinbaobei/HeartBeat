package com.andaily.hb.domain.application;

/**
 * 2019/6/24
 * <p>
 * 实例进行连接的方式，
 * 默认 HTTP
 *
 * @author Shengzhao Li
 * @since 2.0.1
 */
public enum InstanceConnectType {

    HTTP("HTTP"),
    SOCKET_TCP("Socket-TCP"),
    SOCKET_UDP("Socket-UDP");


    private String label;

    InstanceConnectType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
