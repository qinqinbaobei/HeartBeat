package com.andaily.hb.configuration;

import com.andaily.hb.domain.shared.Application;
import com.andaily.hb.web.context.HBLocaleResolver;
import com.andaily.hb.web.context.RegisterUserInterceptor;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.nio.charset.Charset;
import java.util.List;

/**
 * 2018/1/30
 * <p>
 * Spring MVC 扩展配置
 * <p>
 * Replace hb-servlet.xml
 *
 * @author Shengzhao Li
 */
@Configuration
public class MVCConfiguration extends WebMvcConfigurerAdapter {


    /**
     * 扩展拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        //注册校验码
        registry.addInterceptor(new RegisterUserInterceptor()).addPathPatterns("/register.hb");

        super.addInterceptors(registry);
    }


    /**
     * 解决乱码问题
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        super.configureMessageConverters(converters);
        converters.add(new StringHttpMessageConverter(Charset.forName(Application.ENCODING)));
    }


    /**
     * 国际化, localResolver 配置
     */
    @Bean
    public LocaleResolver localeResolver() {
        return new HBLocaleResolver();
    }

    // 国际化配置
    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("/WEB-INF/classes/hb");
        messageSource.setDefaultEncoding(Application.ENCODING);
        return messageSource;
    }


}
