package com.andaily.hb.configuration;

import com.andaily.hb.web.context.HBCharacterEncodingFilter;
import com.andaily.hb.web.context.HBSiteMeshFilter;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

import java.util.concurrent.TimeUnit;

/**
 * 2018/1/30
 * <p>
 * Replace web.xml
 *
 * @author Shengzhao Li
 */
@Configuration
public class WebConfiguration implements EmbeddedServletContainerCustomizer {


    /**
     * 字符编码配置 UTF-8
     */
    @Bean
    public FilterRegistrationBean encodingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new HBCharacterEncodingFilter());
        registrationBean.addUrlPatterns("/*");
        //值越小越靠前
        registrationBean.setOrder(1);
        return registrationBean;
    }


    /**
     * sitemesh filter
     */
    @Bean
    public FilterRegistrationBean sitemesh() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new HBSiteMeshFilter());
        registrationBean.addUrlPatterns("/*");
        //注意: 在 spring security filter之后
        registrationBean.setOrder(8899);
        return registrationBean;
    }


    @Override
    public void customize(ConfigurableEmbeddedServletContainer container) {

        container.setDisplayName("HeartBeat");
        // Session 有效时间: 120分钟
        container.setSessionTimeout(120, TimeUnit.MINUTES);

        container.addErrorPages(
                //404 错误码的配置
                new ErrorPage(HttpStatus.NOT_FOUND, "/static/error/404.html")
        );


    }
}
