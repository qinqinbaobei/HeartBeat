package com.andaily.hb.service.operation.search;

import com.andaily.hb.domain.application.ApplicationInstance;
import com.andaily.hb.domain.application.ApplicationInstanceRepository;
import com.andaily.hb.domain.dto.HBSearchDto;
import com.andaily.hb.domain.dto.HBSearchResultDto;
import com.andaily.hb.domain.shared.BeanProvider;
import com.andaily.hb.domain.shared.paginated.PaginatedLoader;

import java.util.List;
import java.util.Map;

/**
 * 15-3-13
 * <p/>
 * 应用实例的 搜索业务处理
 *
 * @author Shengzhao Li
 */
public class ApplicationInstanceSearcher implements HBSearcher {

    private transient ApplicationInstanceRepository instanceRepository = BeanProvider.getBean(ApplicationInstanceRepository.class);

    public ApplicationInstanceSearcher() {
    }

    @Override
    public HBSearchDto search(HBSearchDto searchDto) {

        final Map<String, Object> map = searchDto.queryMap();
        return searchDto.load(new PaginatedLoader<HBSearchResultDto>() {
            @Override
            public List<HBSearchResultDto> loadList() {
                List<ApplicationInstance> instances = instanceRepository.findHBSearchInstances(map);
                return HBSearchResultDto.toInstanceDtos(instances);
            }

            @Override
            public int loadTotalSize() {
                return instanceRepository.totalHBSearchInstances(map);
            }
        });

    }
}
