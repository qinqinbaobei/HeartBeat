package com.andaily.hb.service.operation.instance;

import com.andaily.hb.domain.application.ApplicationInstance;
import com.andaily.hb.domain.application.ApplicationInstanceRepository;
import com.andaily.hb.domain.application.InstanceConnectType;
import com.andaily.hb.domain.dto.application.ApplicationInstanceFormDto;
import com.andaily.hb.domain.dto.user.WeixinUserDto;
import com.andaily.hb.domain.shared.Application;
import com.andaily.hb.domain.shared.BeanProvider;
import com.andaily.hb.domain.shared.security.SecurityUtils;
import com.andaily.hb.domain.user.UserRepository;
import com.andaily.hb.domain.user.WeixinUser;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * 2016/5/17
 *
 * @author Shengzhao Li
 */
public class ApplicationInstanceFormDtoLoader {


    private transient ApplicationInstanceRepository instanceRepository = BeanProvider.getBean(ApplicationInstanceRepository.class);
    private transient UserRepository userRepository = BeanProvider.getBean(UserRepository.class);

    private String guid;

    public ApplicationInstanceFormDtoLoader(String guid) {
        this.guid = guid;
    }


    /**
     * ConnectType.HTTP
     *
     * @return ApplicationInstanceFormDto
     */
    public ApplicationInstanceFormDto load() {

        ApplicationInstanceFormDto formDto;

        if (StringUtils.isNotEmpty(guid)) {
            formDto = loadFormDto();
        } else {
            formDto = newFromDto();
        }

        final List<WeixinUser> weixinUsers = userRepository.findWeixinUsersByUsername(SecurityUtils.currentUsername());
        formDto.setWeixinUserDtos(WeixinUserDto.toDtos(weixinUsers));

        //设置系统的 proxy是否启用
        formDto.setProxyEnabled(Application.systemSetting().proxyEnabled());

        return formDto;
    }

    private ApplicationInstanceFormDto newFromDto() {
        ApplicationInstanceFormDto formDto = new ApplicationInstanceFormDto();
        formDto.setConnectType(InstanceConnectType.HTTP);
        return formDto;
    }

    private ApplicationInstanceFormDto loadFormDto() {
        ApplicationInstance instance = instanceRepository.findByGuid(guid, ApplicationInstance.class);
        if (instance.enabled()) {
            throw new IllegalStateException("Only Disabled ApplicationInstance support edit");
        }
        ApplicationInstanceFormDto formDto = new ApplicationInstanceFormDto(instance);

        List<String> weixinUserGuids = userRepository.findWeixinUserGuids(instance.guid());
        formDto.setWeixinUserGuids(weixinUserGuids);
        return formDto;
    }
}
